import javax.swing.JOptionPane;

public class Assignment1 
{
	public static void main(String[] agrs)
	{
		//Set normal format of date as String
		String d =
				JOptionPane.showInputDialog("Enter date in the format month/day/year (xx/xx/xx)");
		//classify month,day,year from the input
		String month = d.substring(0,2);
		String day = d.substring(3, 5);
		String year = d.substring(6, 8);
		//set output in European format with classified month,day,year
		JOptionPane.showMessageDialog(null,"your date in European format is " + day + "."+month+"."+year);
	}
}
