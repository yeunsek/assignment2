/*
 * Author: Eun Suk Lee
 * Email:3l24336@email.vccs.edu
 * Date: 2/28/2015
 * This program will calculate maximum, minimum, summation,average, median of the 5 numbers.
 */

import java.util.Arrays;
import java.util.Scanner;

public class Assignment2 
{
	public static void main(String[] args) 
	{
		Scanner s = new Scanner(System.in);
		s.useDelimiter(",|\r");
		
		System.out.println("Enter any five numbers: ");
		int n1 = s.nextInt();
		int n2 = s.nextInt();
		int n3 = s.nextInt();
		int n4 = s.nextInt();
		int n5 = s.nextInt();

		int max = Math.max(n1,Math.max(n2,Math.max(n3,Math.max(n4,n5)))); //maximum
		int min = Math.min(n1,Math.min(n2,Math.min(n3,Math.min(n4,n5)))); //minimum
		int sum = n1+n2+n3+n4+n5; //summation
		double a = sum/5.00; //average
		
		int[] r = {n1,n2,n3,n4,n5}; 
		Arrays.sort(r); //arrange the numbers
		int mid = r.length/2; //get median
		int m = r[mid];
		
		System.out.println ();
		System.out.println ("Maximum: "+max);
		System.out.println ("Minimum: "+min);
		System.out.println ("Sum: "+sum);
		System.out.println ("Average: "+a);
		System.out.println ("Median " + m);
		
		s.close();
	}
}
